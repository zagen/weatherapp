package com.example.weatherapptest;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;



/**
 * manage storing, loading and scaling images
 * @author zagen
 *
 */
public class ImagesManager {
	private final static String IMAGE_COLD_LOCALNAME_STRING = "image_cold_big.jpg";
	private final static String IMAGE_WARM_LOCALNAME_STRING = "image_warm_big.jpg";
	private final static String IMAGE_COLD_LOCALNAME_SMALL_STRING = "image_cold_small.jpg";
	private final static String IMAGE_WARM_LOCALNAME_SMALL_STRING = "image_warm_small.jpg";
	
	public interface ImagesLoadedListener{
		void onImagesLoaded(boolean isLoaded);
	}
	
	/**
	 * called when images are ready
	 */
	private ImagesLoadedListener callback = null;
	
	/**
	 * path to folder with cache
	 */
	private String path;
	
	private class RetrieveImageTask extends AsyncTask<Void, Void, Boolean> {
		
		private final static String IMAGE_COLD_URL_STRING = "http://hikingartist.files.wordpress.com/2012/05/1-christmas-tree.jpg";
		private final static String IMAGE_WARM_URL_STRING = "http://www.rewalls.com/images/201201/reWalls.com_59293.jpg";


		@Override
		protected Boolean doInBackground(Void... params) {
			boolean loaded = true;
			String[] urls = new String[]{ IMAGE_COLD_URL_STRING, IMAGE_WARM_URL_STRING};
			String[] localNames = new String[] {IMAGE_COLD_LOCALNAME_STRING, IMAGE_WARM_LOCALNAME_STRING };
			String[] smallLocalNames = new String[]{IMAGE_COLD_LOCALNAME_SMALL_STRING, IMAGE_WARM_LOCALNAME_SMALL_STRING};
			try {
				for(int i = 0; i < urls.length; i++){
			        URL url = new URL(urls[i]);
			        URLConnection connection = url.openConnection();
			        connection.connect();

			        // download the file
			        InputStream input = new BufferedInputStream(url.openStream());
			        String path = ImagesManager.this.path + localNames[i];
			        OutputStream output = new FileOutputStream(path);

			        byte data[] = new byte[1024];
			        int count;
			        while ((count = input.read(data)) != -1) {
			            output.write(data, 0, count);
			        }

			        output.flush();
			        
			        output.close();
			        input.close();
			        //scale and save scaling images
			        saveBitmapToSDCard(decodeSampledBitmapFromFile(path, 250, 250), smallLocalNames[i]);
				}

		        
		    } catch (Exception e) {
		    	e.printStackTrace();
		    	loaded = false;
		    }

			return loaded;
		}
		@Override
		protected void onPostExecute(Boolean loaded) {
			super.onPostExecute(loaded);
			if(callback != null){
				callback.onImagesLoaded(loaded);
			}
			
		}


	}
	public ImagesManager(){
		//check if directory exist(or create it)
		 
		path = Environment.getExternalStorageDirectory().getAbsoluteFile() + File.separator + "weather_cache" + File.separator;
		File cacheDir = new File(path);
		if(!cacheDir.isDirectory()){
			cacheDir.mkdir();
		}
	}
	public void setImagesLoadedListener(ImagesLoadedListener listener){
		this.callback = listener;
	}
	/**
	 * @return true if scaled images in cache
	 */
	public boolean allImagesExist(){
		String smallColdPath = path + IMAGE_COLD_LOCALNAME_SMALL_STRING;
		String smallWarmPath = path + IMAGE_WARM_LOCALNAME_SMALL_STRING;
		File fileSmallCold = new File(smallColdPath);
		File fileSmallWarm = new File(smallWarmPath);
		
		return fileSmallCold.exists() && fileSmallWarm.exists();
	}
	
	public void loadImages(){
		new RetrieveImageTask().execute();
	}
	
	
	public Bitmap getSmallBitmapCold()
	{
		String smallColdPath = path + IMAGE_COLD_LOCALNAME_SMALL_STRING;
		return BitmapFactory.decodeFile(smallColdPath);
	}
	public Bitmap getSmallBitmapWarm()
	{
		String smallWarmPath = path + IMAGE_WARM_LOCALNAME_SMALL_STRING;
		return BitmapFactory.decodeFile(smallWarmPath);
	}
	

	public static int calculateInSampleSize(
	        BitmapFactory.Options options, int reqWidth, int reqHeight) {
		    // Raw height and width of image
		    final int height = options.outHeight;
		    final int width = options.outWidth;
		    int inSampleSize = 1;
		
		    if (height > reqHeight || width > reqWidth) {
		
		        final int halfHeight = height / 2;
		        final int halfWidth = width / 2;
		
		        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
		        // height and width larger than the requested height and width.
		        while ((halfHeight / inSampleSize) > reqHeight
		                && (halfWidth / inSampleSize) > reqWidth) {
		            inSampleSize *= 2;
		        }
		    }

		    return inSampleSize;
		}
		public Bitmap decodeSampledBitmapFromFile(String fullPath,
		        int reqWidth, int reqHeight) {

		    // First decode with inJustDecodeBounds=true to check dimensions
		    final BitmapFactory.Options options = new BitmapFactory.Options();
		    options.inJustDecodeBounds = true;
		    BitmapFactory.decodeFile(fullPath,  options);

		    // Calculate inSampleSize
		    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		    // Decode bitmap with inSampleSize set
		    options.inJustDecodeBounds = false;
		    return BitmapFactory.decodeFile(fullPath,  options);
		}
		
		public void saveBitmapToSDCard(Bitmap bitmap, String name){
			FileOutputStream out = null;
			try {
			    out = new FileOutputStream(path +  name);
			    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
			} catch (Exception e) {
			    e.printStackTrace();
			} finally {
			    try {
			        if (out != null) {
			            out.close();
			        }
			    } catch (IOException e) {
			        e.printStackTrace();
			    }
			}
		}
}

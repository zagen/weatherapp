package com.example.weatherapptest;


import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * display list of all cities
 * @author zagen
 *
 */
public class CityListFragment extends ListFragment {
	
	/**
	 * array of cities id(parse from xml)
	 */
	private String[] ids;
	
	/**
	 * called when list item clicked
	 */
	OnCitySelectedListener callback;

	/**
	 * name of file on sdcard(cache)
	 */
	private String xmlFilename  = "cities.xml";
	
	
	public interface OnCitySelectedListener {
        public void onCitySelected(String id);
    }
	/**
	 * manage cache of xml files
	 */
	private XmlCacheManager cacheManager = new XmlCacheManager();
	
	
	/**
	 * async task load from server xml file(or take from cache) with cities, parse it and save in fields
	 * return map with 2 arrays - ids and names of cities
	 * @author zagen
	 *	
	 */
	private class RetrieveCitiesTask extends AsyncTask<Void, Void, HashMap<String, String[]>> {
		public static final String CITIES_NAMES = "names";
		public static final String CITIES_IDS = "ids";
		
		private String xmlUrlString = "https://pogoda.yandex.ru/static/cities.xml";

		@Override
		protected HashMap<String, String[]> doInBackground(Void... params) {
			
			HashMap<String, String[]> cities = new HashMap<String, String[]>();
			String[] citiesNames;
			String[] citiesIds;
			
			try {
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();

				//first try take xml file from cache
				InputStream input = cacheManager.getXML(xmlFilename);
				Document doc;

				if(input != null){
					doc = builder.parse(input); 
				}else{
					//if file isn't in a cache, load it from server
					HttpGet uri = new HttpGet(xmlUrlString);    
					DefaultHttpClient client = new DefaultHttpClient();
					HttpResponse resp;
					resp = client.execute(uri);
					StatusLine status = resp.getStatusLine();
					if (status.getStatusCode() != 200) {
					    Log.d("Error", "HTTP error, invalid server status code: " + resp.getStatusLine());  
					}
					cacheManager.saveXML(resp.getEntity().getContent(), xmlFilename);
					input = cacheManager.getXML(xmlFilename);
					doc = builder.parse(input);
				}
				
				NodeList nodeList = doc.getElementsByTagName("city");
				int citiesCount = nodeList.getLength();
				citiesNames = new String[citiesCount];
				citiesIds = new String[citiesCount];
				//take all needed info about cities in 2 array 
				for(int i = 0; i < citiesCount; i++){
					Element element = (Element)nodeList.item(i);
					String name = element.getTextContent();
					String id = element.getAttribute("id");
					citiesNames[i] = name;
					citiesIds[i] = id;
					
				}
				//and put it in a map
				cities.put(CITIES_NAMES, citiesNames);
				cities.put(CITIES_IDS, citiesIds);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch( ParserConfigurationException e){
				e.printStackTrace();
			} catch( SAXException e){
				e.printStackTrace();
			}

			return cities;
		}
		@Override
		protected void onPostExecute(HashMap<String, String[]> result) {
			super.onPostExecute(result);
			//take info from map and fill list
			if(result.containsKey(CITIES_NAMES) && result.containsKey(CITIES_IDS)){
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
				        android.R.layout.simple_list_item_1, result.get(CITIES_NAMES));
				setListAdapter(adapter);
				ids = result.get(CITIES_IDS);
			}
			
		}
		

	}
	  @Override
	  public void onActivityCreated(Bundle savedInstanceState) {
	    super.onActivityCreated(savedInstanceState);

	    new RetrieveCitiesTask().execute();
	  }
	  
	  @Override
	  public void onAttach(Activity activity) {
	        super.onAttach(activity);
	        
	        // This makes sure that the container activity has implemented
	        // the callback interface. If not, it throws an exception
	        try {
	            callback = (OnCitySelectedListener) activity;
	        } catch (ClassCastException e) {
	            throw new ClassCastException(activity.toString()
	                    + " must implement OnCitySelectedListener");
	        }
	        //check cache and internet connection. In worst case show alert 
	        if(!cacheManager.valid(xmlFilename) && !NetworkManager.isNetworkAvailable(activity)){
	        	new AlertDialog.Builder(getActivity())
	            .setTitle(R.string.no_connection_and_no_cache_dialog_title)
	            .setMessage(R.string.no_connection_and_no_cache_dialog_info)
	            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface dialog, int which) { 
	                   getActivity().finish();
	                }
	             })
	            .setCancelable(false)
	            .setIcon(android.R.drawable.ic_dialog_alert)
	             .show();


	        }
	    }
	  
	   @Override
	    public void onListItemClick(ListView l, View v, int position, long id) {
	    	
	    	//take city id and if there is connection load info from server or take from cache, otherwise show alert
	    	String cityId = "";
	    	if(position < ids.length){
	    		cityId = ids[position];
	    	}
	    	if(cityId.length() > 0)
	    		if(NetworkManager.isNetworkAvailable(getActivity()) || cacheManager.valid(cityId+ ".xml"))
	    			callback.onCitySelected(cityId);
	    		else
		        	new AlertDialog.Builder(getActivity())
		            .setTitle(R.string.no_connection_and_no_cache_dialog_title)
		            .setMessage(R.string.no_connection_and_no_cache_dialog_info)
		            .setPositiveButton(android.R.string.yes, null)
		            .setIcon(android.R.drawable.ic_dialog_alert)
		             .show();

	    }
}

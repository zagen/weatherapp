package com.example.weatherapptest;


import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class CityWeatherFragment extends Fragment {
	private ProgressBar loadProgressBar;
	private TextView weatherTextView;
	private ImageView weatherImageView;
	
	private XmlCacheManager cacheManager = new XmlCacheManager();
	
	private ImagesManager imagesManager = new ImagesManager();
	
	private class RetrieveWeatherTask extends AsyncTask<String, Void, String> {

		/**
		 * full url for load xml from server
		 */
		private String xmlUrlFullString;
		
		/**
		 * filename for storing xml in cache
		 */
		private String xmlUrlFilenameString;
		private String xmlUrlPrefix = "http://export.yandex.ru/weather-ng/forecasts/";

		
		@Override
		protected String doInBackground(String... params) {
			    
			String weatherText = null;
			xmlUrlFullString = xmlUrlPrefix + params[0] + ".xml";
			xmlUrlFilenameString = params[0] + ".xml";
			try {
				
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				InputStream input = cacheManager.getXML(xmlUrlFilenameString);
				Document doc;
				
				//check first if xml exist and valid in cache
				if(input != null){
					doc = builder.parse(input); 
				}else{
					//or load from server
					HttpGet uri = new HttpGet(xmlUrlFullString);    
					DefaultHttpClient client = new DefaultHttpClient();
					HttpResponse resp;
					resp = client.execute(uri);
					StatusLine status = resp.getStatusLine();
					if (status.getStatusCode() != 200) {
					    Log.d("Error", "HTTP error, invalid server status code: " + resp.getStatusLine());  
					}
					cacheManager.saveXML(resp.getEntity().getContent(), xmlUrlFilenameString);
					input = cacheManager.getXML(xmlUrlFilenameString);
					doc = builder.parse(input);
				}
				//take temperature and weather type for now from xml 
				String temperature = doc.getElementsByTagName("temperature").item(0).getTextContent();
				String weatherType = doc.getElementsByTagName("weather_type").item(0).getTextContent();
				weatherText = String.format("%s, %s", temperature, weatherType);

			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch( ParserConfigurationException e){
				e.printStackTrace();
			} catch( SAXException e){
				e.printStackTrace();
			}

			return weatherText;
		}
		@Override
		protected void onPostExecute(final String weather) {
			super.onPostExecute(weather);
			
			if(weather != null && weather.length() > 0){
				//check if images are in cache 
				if(!imagesManager.allImagesExist()){
					//otherwise load from server and scaling 
					imagesManager.setImagesLoadedListener(new ImagesManager.ImagesLoadedListener() {
						
						@Override
						public void onImagesLoaded(boolean isLoaded) {
							//then show it 
							setImage(weather);
							setUiVisibility(View.VISIBLE);
							loadProgressBar.setVisibility(View.GONE);
						}
					});
					imagesManager.loadImages();
				}else{
					setImage(weather);
					setUiVisibility(View.VISIBLE);
					loadProgressBar.setVisibility(View.GONE);
				}
				
			}else{
				loadProgressBar.setVisibility(View.GONE);
			}
			
			
			
		}
		@Override
		protected void onPreExecute() {

			loadProgressBar.setVisibility(View.VISIBLE);
			setUiVisibility(View.GONE);
			super.onPreExecute();
		}
		

	}
	
	private RetrieveWeatherTask loadTask = null;
	public void loadWeatherInCityWithId(String id){
		if(id != null && id.length() > 0){
			loadTask = new RetrieveWeatherTask();
			loadTask.execute(id);
		}
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.detailed_weather_frag_layout, container, false);
		this.loadProgressBar = (ProgressBar)rootView.findViewById(R.id.progressBar);
		this.weatherTextView = (TextView)rootView.findViewById(R.id.weatherText);
		this.weatherImageView = (ImageView)rootView.findViewById(R.id.weatherImage);
		return rootView;
	}
	
	
	@Override
	public void onDetach() {
		super.onDetach();
		if(loadTask != null)
			loadTask.cancel(true);
	}
	private void setUiVisibility(int visibility){
		weatherTextView.setVisibility(visibility);
		weatherImageView.setVisibility(visibility);
	}
	private void setImage(String weather){
		String temperature = weather.split(",")[0];
		TextView weatherTextView = (TextView) getView().findViewById(R.id.weatherText);
		weatherTextView.setText(weather);
		
		if(temperature.startsWith("-")){
			//load picture for negative temperature
			weatherImageView.setImageBitmap(imagesManager.getSmallBitmapCold());
		}else{
			//load picture for +  
			weatherImageView.setImageBitmap(imagesManager.getSmallBitmapWarm());
		}
	}
}

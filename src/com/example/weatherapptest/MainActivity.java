package com.example.weatherapptest;
import android.app.Activity;
import android.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity implements CityListFragment.OnCitySelectedListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Configuration config = getResources().getConfiguration();
		
		//show correct layout on portrait and landscape mode 
	    if(config.orientation == Configuration.ORIENTATION_PORTRAIT){
	    	setContentView(R.layout.activity_main_portrait);
	    }else{
	    	setContentView(R.layout.activity_main_landscape);
	    }	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCitySelected(String id) {
		Fragment fragment = getFragmentManager().findFragmentById(R.id.weatherFragmentId);
		if (fragment instanceof CityWeatherFragment) {
			CityWeatherFragment weatherFragment = (CityWeatherFragment) fragment;
			weatherFragment.loadWeatherInCityWithId(id);
		}
		
		
	}
}

package com.example.weatherapptest;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;

import android.os.Environment;

/**
 * manage storing cache of xml files on sdcard
 * @author zagen
 *
 */
public class XmlCacheManager {
	private String path;
	
	/**
	 * public constructor
	 */
	public XmlCacheManager(){
		path = Environment.getExternalStorageDirectory().getAbsoluteFile() + File.separator + "weather_cache" + File.separator;
		File cacheDir = new File(path);
		if(!cacheDir.isDirectory()){
			cacheDir.mkdir();
		}
	}
	
	/**
	 * 
	 * @param filename
	 * @return true if file in cache and up-to-date
	 */
	public boolean valid(String filename){
		File file = new File(path + filename);
		if(file.exists()){
			Date lastModified = new Date(file.lastModified());
			Calendar c = Calendar.getInstance(); 
			c.set(Calendar.HOUR, c.get(Calendar.HOUR) - 1);
			Date dateHourBeforeNow = c.getTime();
			if(lastModified.after(dateHourBeforeNow))
			{
				return true;
			}
		}
		return false;
	}
	/**
	 * Open xml file from sdcard if it valid 
	 * @param filename
	 * @return
	 */
	public InputStream getXML(String filename){
		InputStream input = null;
		if(valid(filename)){
			try {
				input = new BufferedInputStream(new FileInputStream(path + filename));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		return input;
	}
	/**
	 * save data from input stream on sd card
	 * @param input
	 * @param filename
	 */
	public void saveXML(InputStream input, String filename){
		OutputStream output;
		try {
			output = new FileOutputStream(path + filename);
		
	
	        byte data[] = new byte[1024];
	        int count;
	        while ((count = input.read(data)) != -1) {
	            output.write(data, 0, count);
	        }
	        output.flush();
	        output.close();
	        input.close();
        
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		}


	}
}
